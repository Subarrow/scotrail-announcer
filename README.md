This project uses the [recently published](https://matteason.github.io/scotrail-announcements-june-2022/)
ScotRail announcements to narrate actual departures in real-time from the Scottish station of your
choosing.

At the moment, all it announces are upcoming departures, and platform alterations.

## Map
I'm not going to publish the scripts I used to generate the mappings for sound
files out of sheer shame, but please feel free to use the mappings I generated
in your own projects, they're in `sr_announcements_map_export.txt`.

If you want a bit more insight into how this fits into data, you might want to
read this [post](https://evelyn.moe/station-announcements-from-a-spreadsheet-and-some-files-back-to-announcements).

## External dependencies
You'll need to install sox and aplay for this to work, you can likely find
these in your distro's repos.

## Setup
Clone the repository, cd into the directory, now create a virtual environment,
activate it, and install suds from the requirements file.

```
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
```

You'll need to register for LDBSVWS, which you can do [here](http://openldbsv.nationalrail.co.uk/). Take the
security token and put it alone in a file called `secret.txt`.

## Running

Activate the virtual environment, if you have one, pick the CRS code for your
station (e.g. EDB for Edinburgh), and run:
`./announcer.py EDB`

## Licence
The software in this project is licensed under the MIT licence, see LICENCE.txt
for more information.

It's not fully clear who holds the copyright to the audio files. It could be
that it's Scotrail themselves, who set out the below conditions for the use
of material released under their FOISA publication programme.

```
Where ScotRail Trains Limited holds the copyright in its published information,
the information may be copied or reproduced without formal permission, provided
that:

- it is copied or reproduced accurately.
- it is not used in a misleading context, and
- the source of the material is identified.

```
