#!/usr/bin/env python3

import os
import string
import logging
import argparse
from time import sleep
from datetime import datetime
from typing import Optional, List, Dict, Any

from suds.client import Client

URL = "https://lite.realtime.nationalrail.co.uk/OpenLDBSVWS/wsdl.aspx?ver=2021-11-01"
CONSIDER_TRAIN_DELAYED_AFTER_MINUTES = 10

services_by_rid = {}
service_rids_announced = set()
selected_chime = ""

logger = logging.getLogger("Announcer")


def setup_logger() -> None:
    global logger
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    logger = logging.getLogger("Announcer")
    logger.setLevel(logging.DEBUG)
    logger.propagate = False

    log_format = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s", '%Y-%m-%dT%H:%M:%S%z')
    ch.setFormatter(log_format)
    logger.addHandler(ch)


def now() -> str:
    """Datetime formatted in the way LDBWS wants it"""
    return datetime.now().isoformat().split(".")[0]


def resolve(*files: str) -> Optional[List[str]]:
    prefixed_files = [f"audio/{a}.wav" for a in files]

    for filename in prefixed_files:
        if not os.path.exists(filename):
            logger.warning("Could not stat " + filename)

    if all(os.path.exists(a) for a in prefixed_files):
        return prefixed_files
    else:
        return None


def stations_for(station_list: List[str], only: bool = False, end: str = "END"):
    if len(station_list) == 1 and only:
        return ["STN_" + station_list[0] + "_MID", "WORD_ONLY"]
    elif len(station_list) == 1:
        return ["STN_" + station_list[0] + f"_{end}"]
    else:
        station_list = [f"STN_{a}_MID" for a in station_list]
        station_list.insert(-1, "WORD_AND")
        station_list[-1] = station_list[-1][:-3] + end
        return station_list


def is_delayed_dep(service: dict) -> bool:
    """Determines if a train is delayed by at least 10 minutes"""
    time_scheduled = service["times"]["departure"]["scheduled"]
    time_estimated = service["times"]["departure"]["estimated"]

    if not time_scheduled or not time_estimated:
        return False
    if (time_estimated - time_scheduled).seconds > 60*CONSIDER_TRAIN_DELAYED_AFTER_MINUTES:
        return True
    return False


def time_for(dt: Optional[datetime]) -> Optional[List[str]]:
    if dt is None:
        return None
    elif dt.hour == 0 and dt.minute == 0:
        return ["WORD_MIDNIGHT"]
    elif dt.minute == 0:
        return ["TIME_" + dt.strftime("%H"), "HUNDRED_HOURS"]
    else:
        return ["TIME_" + dt.strftime("%H"), "TIME_LAST_" + dt.strftime("%M")]


def platform_for(platform: str, crs: str, part: str) -> Optional[List[str]]:
    """This is the last resort option when we didn't match the platform in the nice way"""
    if platform is None:
        return None

    # Edinburgh has E/W suffixed platforms announced as East and West respectively
    if crs == "EDB":
        final = []
        if platform.endswith("W"):
            final = [f"WORD_CUT_WEST_{part}"]
        elif platform.endswith("E"):
            final = [f"WORD_CUT_EAST_{part}"]

        platform = platform.rstrip(string.ascii_letters)
        if final:
            return [f"WORD_PLATFORM", f"NUMBER_{platform}_MID"] + final
        else:
            return [f"WORD_PLATFORM", f"NUMBER_{platform}_{part}"]
    else:
        if platform[0].isnumeric():
            platform = platform.rstrip(string.ascii_letters)
            return [f"WORD_PLATFORM", f"NUMBER_{platform}_{part}"]
        else:
            return [f"WORD_PLATFORM", f"NUMBER_{platform}_{part}"]


def attempt_to_announce(ann_type: str, service: dict) -> None:
    announcement = None

    logger.info(f"{ann_type} {service}")

    if ann_type == "reminder":
        announcement = []

        if is_delayed_dep(service):
            announcement.append(
                resolve(f"PLAT_{service['platform']}_FOR_DELAYED") or
                resolve(*platform_for(service["platform"], service["crs"], "MID"), f"FOR_THE", f"WORD_DELAYED"))
        else:
            announcement.append(
                resolve(f"PLAT_{service['platform']}_FOR") or
                resolve(*platform_for(service["platform"], service["crs"], "MID"), f"FOR_THE"))

        announcement.extend([
            resolve(*time_for(service["times"]["departure"]["scheduled"])),
            resolve(f"OPERATOR_{service['operator']}_SERVICE_TO") or resolve("SERVICE_TO"),
            resolve(*stations_for(service["destinations"])),
            resolve(f"CALLING_AT"),
            resolve(*stations_for(service["calls_at"], only=True)),
            ])
        if service["length"]:
            announcement.append(
                resolve(f"COACHES_{service['length']}") or
                resolve(f"TR_FORMED_OF", f"NUMBER_{service['length']}_END", "WORD_COACHES"))

    elif ann_type == "alteration":
        announcement = [
            resolve("ATTENTION_PLEASE", "PLATFORM_ALTERATION", "WORD_THE"),
            resolve(*time_for(service["times"]["departure"]["scheduled"])),
            resolve(f"OPERATOR_{service['operator']}_SERVICE_TO") or resolve("SERVICE_TO"),
            resolve(*stations_for(service["destinations"], end="MID")),
            resolve("WILL_NOW_DEP_FROM"),
            resolve(
                f"PLAT_{service['platform']}") or
                 resolve(*platform_for(service["platform"], service["crs"], "END")
            ),
            ]
    elif ann_type == "cancellation":
        announcement = [
            resolve("WE_ARE_SORRY_TO_ANNOUNCE"),
            resolve(*time_for(service["times"]["departure"]["scheduled"])),
            resolve(f"OPERATOR_{service['operator']}_SERVICE_TO") or resolve("SERVICE_TO"),
            resolve(*stations_for(service["destinations"], end="MID")),
            resolve("HAS_BEEN_CANCELLED"),
        ]

    if announcement is not None and all(a is not None for a in announcement):
        os.system(
            "sox " +
            " ".join([selected_chime] + [" ".join(a) for a in announcement]) +
            " /tmp/announcement_composed.wav"
            )
        os.system("aplay /tmp/announcement_composed.wav")
    else:
        logger.error("Could not announce {service['rid']} " + str(announcement))


def consider_announcing(service: dict) -> None:
    global services_by_rid, service_rids_announced

    cached_service = services_by_rid.get(service["rid"])
    if cached_service:
        if cached_service["platform"] is not None and service["platform"] is not None and cached_service["platform"] != service["platform"]:
            attempt_to_announce("alteration", service)
        if not cached_service["cancelled"] and service["cancelled"]:
            attempt_to_announce("cancellation", service)

    services_by_rid[service["rid"]] = service

    estimated_time = service["times"]["departure"]["estimated"]

    if estimated_time and (estimated_time - datetime.now()).seconds < 3*60 and service["rid"] not in service_rids_announced:
        attempt_to_announce("reminder", service)
        service_rids_announced.add(service["rid"])


def get_times(service: dict) -> Dict[str, Dict[str, Optional[datetime]]]:
    time_out = {"arrival": {}, "departure": {}, "pass": {}}
    for time_type in time_out.keys():
        for time_subtype in ["estimated", "actual", "scheduled"]:
            time_raw = getattr(service, time_subtype[0] + "t" + time_type[0], None)
            if time_raw:
                time_out[time_type][time_subtype] = datetime.strptime(time_raw, "%Y-%m-%dT%H:%M:%S")
            else:
                time_out[time_type][time_subtype] = None
    return time_out


def normalise_service(service, crs: str) -> Dict[str, Any]:
    return {
        "rid": service.rid,
        "crs": crs,
        "platform": getattr(service, "platform", None),
        "calls_at": [a.tiploc for a in service.subsequentLocations[0]
                     if not hasattr(a, "isPass") and not hasattr(a, "isOperational")],
        "cancelled": hasattr(service, "isCancelled"),
        "operator": service.operatorCode,
        "origins": [a.tiploc for a in service.origin[0]],
        "destinations": [a.tiploc for a in service.destination[0]],
        "times": get_times(service),
        "length": getattr(service, "length", None),
        }


if __name__ == "__main__":
    setup_logger()

    parser = argparse.ArgumentParser()
    parser.add_argument('crs')
    parser.add_argument('--chimes',
                        default='none',
                        const='none',
                        nargs='?',
                        choices=['none', 'sr', 'rzhd'],
                        help='chime played before each announcement (default: %(default)s)')

    args = parser.parse_args()

    selected_chime = {"rzhd": "chimes/CHIMES_RZHD.wav", "sr": "chimes/CHIMES_SR.wav"}.get(args.chimes) or ""

    with open("secret.txt") as f:
        token = f.read().strip()

    client = Client(URL, plugins=[])
    header_token = client.factory.create('ns2:AccessToken')
    header_token.TokenValue = token
    client.set_options(soapheaders=header_token)

    logger.info("SOAP client set up")

    while True:
        query = client.service.GetDepBoardWithDetails(50, args.crs, now(), timeWindow=240)
        board_crs = query.crs

        board_services = getattr(query, "trainServices", None)
        # trainServices will only be nil when there are no services within the time

        for _i in range(6):
            if board_services is not None:
                for individual_service in board_services[0]:
                    consider_announcing(normalise_service(individual_service, board_crs))
            else:
                pass
            sleep(5)

        sleep(5)
